import React, { Component } from 'react';
import {
  Route,
  NavLink,
  HashRouter
} from "react-router-dom";
import Tasks from "./Tasks";
import Descrip from "./Descrip";
import CreateTask from "./CreateTask";
import './App.css';


class App extends Component {



  render() {
    return (
      <HashRouter>
      <div>
        <h1>Final Project</h1>
        <ul className="header">
          <li><NavLink to="/">Home</NavLink></li>
          <li><NavLink to="/tasks">Task</NavLink></li>
        </ul>
        <div className="content">
          <Route path="/" component={Descrip}/>
          <Route path="/tasks" component={Tasks}/>
          <Route path="/task/create" component={CreateTask}/>
        </div>
      </div>
      </HashRouter>
    );
  }
}

export default App;
