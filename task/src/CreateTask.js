
import React, { Component } from 'react';
import axios from 'axios';

class CreateTask extends Component {

    constructor(props) {
        super(props);
        this.onChangeTitle = this.onChangeTitle.bind(this);
        this.onChangeDetail = this.onChangeDetail.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            title: '',
            detail: ''
        }
    }
    onChangeTitle(e) {
        this.setState({
            title: e.target.value
        });
    }
    onChangeDetail(e) {
        this.setState({
            detail: e.target.value
        });
    }
    onSubmit(e) {
        e.preventDefault();
        const serverport = {
            title: this.state.name,
            detail: this.state.port
        }
        axios.post('https://jsonplaceholder.typicode.com/posts', serverport)
        .then(res => console.log(res.data));

        this.setState({
            title: '',
            detail: ''
        });
    }

    render() {
        return (
            <div style={{marginTop: 50}}>
                <h3>Add Task</h3>
                <form>
                    <div >
                        <label>Title:  </label>
                        <input type="text" />
                    </div>
                    <div >
                        <label>Detail: </label>
                        <input type="text" />
                    </div>
                    <div >
                        <input type="submit" value="Add Task"/>
                    </div>
                </form>
            </div>
        )
    }
}
export default CreateTask;
