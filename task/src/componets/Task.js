import React from "react";
import PropTypes from "prop-types";

function Task(props) {
  return (
    <div >
      <span>{props.name}</span>
    </div>
  );
}

Task.propTypes = {
  name: PropTypes.string.isRequired
};

export default Task;
