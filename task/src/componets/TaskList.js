import React from "react";
import PropTypes from "prop-types";

// import the Contact component
import Task from "./Task";

function TaskList(props) {
  return (
    <div>{props.contacts.map(c => <Task key={c.id} name={c.name} />)}</div>
  );
}

TaskList.propTypes = {
  contacts: PropTypes.array.isRequired
};

export default TaskList;
