import React, { Component } from "react";
import axios from "axios";
import {
  Route,
  NavLink,
  HashRouter
} from "react-router-dom";

import TaskList from "./componets/TaskList";

class Tasks extends Component {
  state = {
    tasks: []
  };

  componentDidMount() {
    axios
      .get("https://jsonplaceholder.typicode.com/users")
      .then(response => {
        // create an array of contacts only with relevant data
        const newTask = response.data.map(c => {
          return {
            id: c.id,
            title: c.name
          };
        });

        // create a new "state" object without mutating
        // the original state object.
        const newState = Object.assign({}, this.state, {
          tasks: newTask
        });

        // store the new state object in the component's state
        this.setState(newState);
      })
      .catch(error => console.log(error));
}
  render() {
    return (
      <div>
        <h2>Task</h2>
        <NavLink to="/task/create">Create New Task</NavLink>
        <p>Lista de tareas</p>
        <TaskList contacts={this.state.tasks} />
      </div>
    );
  }
}

export default Tasks;
